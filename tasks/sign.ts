import { task } from "hardhat/config";

interface TaskArgs {
  n: number;
  p: string;
  s: string;
}

task("sign", "Create auth key")
  .addParam("n", "Unique nonce")
  .addParam("p", "Route path")
  .addParam("s", "Salt")
  .setAction(async (args: TaskArgs, hre) => {
    const hash = hre.ethers.utils.solidityKeccak256(
      ["string", "string", "uint256"],
      [args.p, args.s, args.n]
    );
    const [signer] = await hre.ethers.getSigners();
    const signature = await signer.signMessage(hre.ethers.utils.arrayify(hash));
    console.log("Signature:", signature);
  });
