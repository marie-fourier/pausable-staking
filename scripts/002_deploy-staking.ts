import { ethers } from "hardhat";

async function main() {
  if (!process.env.TOKEN_ADDRESS) {
    throw new Error("set TOKEN_ADDRESS in .env");
  }

  const StakingFactory = await ethers.getContractFactory("Staking");
  const staking = await StakingFactory.deploy(
    String(process.env.TOKEN_ADDRESS)
  );

  await staking.deployed();

  console.log("Contract deployed to:", staking.address);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
