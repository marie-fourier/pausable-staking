import { ethers } from "hardhat";

async function main() {
  const Token = await ethers.getContractFactory("Token");
  const token = await Token.deploy();

  await token.deployed();

  console.log("Token deployed to:", token.address);

  const StakingFactory = await ethers.getContractFactory("Staking");
  const staking = await StakingFactory.deploy(token.address);

  await staking.deployed();

  console.log("Contract deployed to:", staking.address);

  const accounts = (await ethers.getSigners()).slice(0, 5);

  for (const account of accounts) {
    await token.mint(account.address, ethers.utils.parseEther("1000000000"));
    await token
      .connect(account)
      .approve(staking.address, ethers.utils.parseEther("1000000000"));
    for (let i = 0; i < 10; ++i) {
      await staking.connect(account).stake(ethers.utils.parseEther("10"));
      await staking.connect(account).unstake();
    }
  }
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
