import { ethers } from "hardhat";

async function main() {
  if (!process.env.CONTRACT_ADDRESS) {
    throw new Error("set CONTRACT_ADDRESS in .env");
  }

  const token = await ethers.getContractAt(
    "Token",
    String(process.env.TOKEN_ADDRESS)
  );
  const staking = await ethers.getContractAt(
    "Staking",
    String(process.env.CONTRACT_ADDRESS)
  );

  await token.approve(staking.address, ethers.utils.parseEther("1000000000"));
  await staking.stake(ethers.utils.parseEther("10"));
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
