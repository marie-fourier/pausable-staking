import { expect, assert } from "chai";
import { ethers } from "hardhat";

describe("Staking", function () {
  let owner: any, account: any, token: any, staking: any;

  before(async () => {
    const Token = await ethers.getContractFactory("Token");
    const Staking = await ethers.getContractFactory("Staking");
    [owner, account] = await ethers.getSigners();
    token = await Token.deploy();

    await token.deployed();

    staking = await Staking.deploy(token.address);

    await staking.deployed();

    await token.approve(staking.address, ethers.utils.parseEther("1000000"));

    await token.mint(account.address, ethers.utils.parseEther("1000000"));
    await token
      .connect(account)
      .approve(staking.address, ethers.utils.parseEther("1000000"));
  });

  it("shoult not stake less than 10 and more than 10kk tokens", async () => {
    await expect(
      staking.stake(ethers.utils.parseEther("1"))
    ).to.be.revertedWith("Staking: invalid amount");
    await expect(
      staking.stake(ethers.utils.parseEther("1000001"))
    ).to.be.revertedWith("Staking: invalid amount");
  });

  it("stake should emit event and record history", async () => {
    await expect(() =>
      staking.stake(ethers.utils.parseEther("500"))
    ).to.changeTokenBalance(token, staking, ethers.utils.parseEther("500"));
    await expect(staking.connect(account).stake(ethers.utils.parseEther("500")))
      .to.emit(staking, "Stake")
      .withArgs(account.address, ethers.utils.parseEther("500"));

    let history = await staking.getWalletHistory(owner.address);
    let json = JSON.parse(history);
    expect(json).to.have.length(1);
    assert(
      json[0]["staking-timestamp"] !== "0" &&
        json[0]["staked-amount"] === "500000000000000000000"
    );

    history = await staking.getWalletHistory(account.address);
    json = JSON.parse(history);
    expect(json).to.have.length(1);
    assert(
      json[0]["staking-timestamp"] !== "0" &&
        json[0]["staked-amount"] === "500000000000000000000"
    );

    json.forEach((element: any) => {
      expect(element).haveOwnProperty("staked-amount");
      expect(element).haveOwnProperty("staking-timestamp");
      expect(element).haveOwnProperty("unstaking-timestamp");
    });
  });

  it("should not stake twice", async () => {
    await expect(
      staking.stake(ethers.utils.parseEther("100"))
    ).to.be.revertedWith("Staking: already staked");
  });

  it("should not stake while paused", async () => {
    await staking.pause();
    await expect(
      staking.stake(ethers.utils.parseEther("100"))
    ).to.be.revertedWith("Pausable: paused");
    await staking.unpause();
  });

  it("only owner should pause and unpause", async () => {
    await expect(staking.connect(account).pause()).to.be.revertedWith(
      "Ownable: caller is not the owner"
    );
    await expect(staking.connect(account).unpause()).to.be.revertedWith(
      "Ownable: caller is not the owner"
    );
  });

  it("unstake should emit event and record history", async () => {
    await expect(() => staking.unstake()).to.changeTokenBalance(
      token,
      owner,
      ethers.utils.parseEther("500")
    );
    await expect(staking.connect(account).unstake())
      .to.emit(staking, "Unstake")
      .withArgs(account.address, ethers.utils.parseEther("500"));

    let history = await staking.getWalletHistory(owner.address);
    let json = JSON.parse(history);
    assert(json.length === 1);
    assert(
      json[0]["unstaking-timestamp"] !== "0" &&
        json[0]["staked-amount"] === "500000000000000000000"
    );

    history = await staking.getWalletHistory(account.address);
    json = JSON.parse(history);
    assert(json.length === 1);
    assert(
      json[0]["unstaking-timestamp"] !== "0" &&
        json[0]["staked-amount"] === "500000000000000000000"
    );
  });

  it("should not unstake twice", async () => {
    await expect(staking.unstake()).to.be.revertedWith(
      "Staking: nothing staked"
    );
  });

  it("should return all wallets", async () => {
    const wallets = await staking.getAllWallets();
    expect(wallets).to.have.length(2);
    expect(wallets).to.include.members([owner.address, account.address]);
  });

  it("should return all history", async () => {
    const history = await staking.getAllWalletsHistory();
    const json = JSON.parse(history);
    expect(Object.keys(json)).to.include.members([
      owner.address.toLowerCase(),
      account.address.toLowerCase(),
    ]);
    json[owner.address.toLowerCase()].forEach((element: any) => {
      expect(element).haveOwnProperty("staked-amount");
      expect(element).haveOwnProperty("staking-timestamp");
      expect(element).haveOwnProperty("unstaking-timestamp");
    });
  });
});
