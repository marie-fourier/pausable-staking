# Requirements
### Write a smart contract with the following features
-  (✅) A function that allows the user to stake an ERC20 token (minimum of 10 tokens and a maximum of 1 million). The contract has to emit an event for every successful staking.
-  (✅) A function that lets you un-stake the token. The contract has to emit an event for every successful un-staking.
-  (✅) A function that provides you details of all wallets that have staked and un-staked along with the respective timestamps for each action.
-  (✅) A function that lets the admin/owner of the contract configure whether the contract can be used for staking.

### Build a NodeJS API that does the following

-  (✅) An API endpoint to list all staking events of the contract. (Output as a JSON string).
-  (✅) An API endpoint that lists all the un-staking events of the contract. (Output as a
JSON string).
-  (✅) An API endpoint that reports the following
```json
[
  {
    wallet:
    staked-amount:
    staking-timestamp:
    unstaking-timestamp:
  },
]
```

-  (✅*) An API endpoint that allows an elevated user to change whether the contract can be used for staking or not. This would require a transaction. You don’t need to deploy the contracts from the section above, but showing how you would do that is key

To access the endpoint that creates a transaction user must sign a message with the following format `{path}{salt}{unique_nonce}`. The backend then verifies this one-time signatures and ensures that it belongs to a trusted user.

Signature is created via hardhat task `sign`

\
\
** Tested on local hardhat fork of ethereum mainnet