const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const ethers = require("ethers");
const StakingContract = require("../artifacts/contracts/Staking.sol/Staking.json");
const authGuard = require("./auth-middleware");
require("dotenv").config();

const provider = new ethers.providers.JsonRpcProvider("http://localhost:8545");
const contract = new ethers.Contract(
  process.env.CONTRACT_ADDRESS,
  StakingContract.abi,
  provider
);
const signer = new ethers.Wallet(process.env.PRIVATE_KEY, provider);

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.get("/stakes", async (req, res) => {
  const stakeFilter = contract.filters.Stake();
  const events = await contract.queryFilter(stakeFilter);
  return res.json(events);
});

app.get("/unstakes", async (req, res) => {
  const stakeFilter = contract.filters.Unstake();
  const events = await contract.queryFilter(stakeFilter);
  return res.json(events);
});

app.get("/history", async (req, res) => {
  const json = JSON.parse(await contract.getAllWalletsHistory());
  const addresses = Object.keys(json);
  const history = [];
  for (const address of addresses) {
    const actions = json[address];
    for (const action of actions) {
      history.push({
        wallet: address,
        ...action,
      });
    }
  }
  res.json(history);
});

app.post("/pause", authGuard, async (req, res) => {
  const tx = await contract.connect(signer).pause();
  if (tx.hash) {
    res.end(`Created a tx ${tx.hash}`);
  } else {
    res.end("Could not create a tx");
  }
});
app.post("/unpause", authGuard, async (req, res) => {
  const tx = await contract.connect(signer).unpause();
  if (tx.hash) {
    res.end(`Created a tx ${tx.hash}`);
  } else {
    res.end("Could not create a tx");
  }
});
app.use((error, req, res, next) => {
  res.status(500).send(error.message);
});

const port = process.env.PORT || 4000;
app.listen(port, () => {
  console.log("Listening on port", port);
});
