const ethers = require("ethers");

const wallets = ["0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266"];

const usedNonces = [];

const salt = "hehehe";

const authGuard = (req, res, next) => {
  try {
    const nonce = req.body.nonce;
    const signature = req.body.signature;
    const path = req.path.slice(1);
    if (isNaN(nonce) || usedNonces.find((n) => n === nonce)) {
      throw new Error("Unauthorized");
    }

    const hash = ethers.utils.solidityKeccak256(
      ["string", "string", "uint256"],
      [path, salt, Number(nonce)]
    );
    const signer = ethers.utils.verifyMessage(
      ethers.utils.arrayify(hash),
      signature
    );
    if (wallets.find((wallet) => signer === wallet)) {
      usedNonces.push(nonce);
      return next();
    }
    throw new Error("Unauthorized");
  } catch (err) {
    throw new Error("Unauthorized");
  }
};

module.exports = authGuard;
