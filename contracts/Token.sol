// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract Token is Ownable, ERC20 {
  constructor() ERC20("Token", "TKN") {
    _mint(msg.sender, 100_000_000 ether);
  }

  function mint(address addr, uint amount) external onlyOwner {
    _mint(addr, amount);
  }

  function burn(address addr, uint amount) external onlyOwner {
    _burn(addr, amount);
  }
}