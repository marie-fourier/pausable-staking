// SPDX-License-Identifier: Unlicensed
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract Staking is Pausable, Ownable {
  using Strings for uint256;

  struct StakingAction {
    uint256 stakingTimestamp;
    uint256 unstakingTimestamp;
    uint256 stakedAmount;
  }

  event Stake(
    address indexed sender,
    uint256 amount
  );
  event Unstake(
    address indexed sender,
    uint256 amount
  );

  IERC20 private immutable token;
  uint256 private constant MIN_STAKE = 10 * 10**18;
  uint256 private constant MAX_STAKE = 1_000_000 * 10**18;

  address[] private addresses;
  mapping(address => StakingAction[]) private wallets;

  constructor(address _token) {
    token = IERC20(_token);
  }

  /**
   * @notice transfers user's tokens to the contract
   * @param amount amount of tokens
   */
  function stake(uint256 amount) external whenNotPaused {
    require(amount >= MIN_STAKE && amount <= MAX_STAKE, "Staking: invalid amount");

    bool success = token.transferFrom(
      msg.sender,
      address(this),
      amount
    );
    require(success, "Staking: transfer failed");

    StakingAction[] storage actions = wallets[msg.sender];
    if (
      actions.length == 0 ||
      actions[actions.length - 1].unstakingTimestamp != 0
    ) {
      if (actions.length == 0) {
        addresses.push(msg.sender);
      }
      StakingAction memory action;
      action.stakingTimestamp = block.timestamp;
      action.stakedAmount = amount;
      wallets[msg.sender].push(action);
    } else {
      revert("Staking: already staked");
    }
    emit Stake(msg.sender, amount);
  }

  /**
   * @notice transfers staked tokens back
   */
  function unstake() external {
    StakingAction[] storage actions = wallets[msg.sender];
    require(
      actions.length != 0 &&
      actions[actions.length - 1].unstakingTimestamp == 0,
      "Staking: nothing staked"
    );
    StakingAction storage lastAction = actions[actions.length - 1];
    lastAction.unstakingTimestamp = block.timestamp;
    bool success = token.transfer(msg.sender, lastAction.stakedAmount);
    require(success, "Staking: transfer failed");
    emit Unstake(msg.sender, lastAction.stakedAmount);
  }

  /**
   * @notice pauses staking
   */
  function pause() external onlyOwner {
    _pause();
  }

  /**
   * @notice unpauses staking
   */
  function unpause() external onlyOwner {
    _unpause();
  }

  /**
   * @notice returns history of user's actions
   * @param sender sender
   * @return json array of actions
   */
  function getWalletHistory(address sender)
    external
    view
    returns (string memory)
  {
    return _convertActionsToJson(wallets[sender]);
  }

  /**
   * @notice returns all wallets that have staked
   * @return array of wallets
   */
  function getAllWallets()
    external
    view
    returns (address[] memory)
  {
    return addresses;
  }

  /**
   * @notice A function that provides you details of all
   * wallets that have staked and un-staked along with
   * he respective timestamps for each action.
   * @dev returning json string because we can't return mapping 🤔
   * @return json history as json object
   */
  function getAllWalletsHistory()
    external
    view
    returns (string memory json)
  {
    json = "{";
    uint256 length = addresses.length;
    if (length > 0) {
      for (uint256 i = 0; i < length - 1; ++i) {
      json = string(abi.encodePacked(
        json,
        "\"",
        Strings.toHexString(uint160(addresses[i]), 20),
        "\":",
        _convertActionsToJson(
          wallets[addresses[i]]
        )
      ));
      json = string(abi.encodePacked(json, ","));
    }
    json = string(abi.encodePacked(
        json,
        "\"",
        Strings.toHexString(uint160(addresses[length - 1]), 20),
        "\":",
        _convertActionsToJson(
          wallets[addresses[length - 1]]
        )
      ));
    }
    json = string(abi.encodePacked(json, "}"));
  }

  /**
   * @notice convert actions array to json
   */
  function _convertActionsToJson(
    StakingAction[] memory actions
  )
    internal
    pure
    returns (string memory json)
  {
    json = "[";
    if (actions.length > 0) {
      for (uint256 i = 0; i < actions.length - 1; ++i) {
        json = string(abi.encodePacked(json, _convertActionToJson(actions[i])));
        json = string(abi.encodePacked(json, ","));
      }
      json = string(abi.encodePacked(json, _convertActionToJson(actions[actions.length - 1])));
    }
    json = string(abi.encodePacked(json, "]"));
  }

  /**
   * @notice convert action object to json
   */
  function _convertActionToJson(
    StakingAction memory action
  )
    internal
    pure
    returns (string memory json)
  {
    json = "{";
    json = string(abi.encodePacked(
      json,
      "\"staked-amount\":\"",
      action.stakedAmount.toString(),
      "\""
    ));
    json = string(abi.encodePacked(
      json,
      ",\"staking-timestamp\":\"",
      action.stakingTimestamp.toString(),
      "\""
    ));
    json = string(abi.encodePacked(
      json,
      ",\"unstaking-timestamp\":\"",
      action.unstakingTimestamp.toString(),
      "\""
    ));
    json = string(abi.encodePacked(json, "}"));
  }
}
